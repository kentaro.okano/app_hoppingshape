// 初期設定
const initialTop = 800
const initialLeft = 500
const gravity = 9.8
const slowMo = 100
const xVelocuty = {
  min: -120,
  max: -30
}
const yVelocity = {
  min: -20,
  max: 20
}
const color = {
  red: { min: 0, max: 255 },
  green: { min: 255, max: 255 },
  blue: { min: 0, max: 255 }
}

// 親クラス
class shape {
  constructor(shapeObject) {
    this.top = initialTop
    this.left = initialLeft
    this.yVelocity = this.random(xVelocuty.min, xVelocuty.max)
    this.xVelocity = this.random(yVelocity.min, yVelocity.max)
    this.timerId = 0
    this.color = {
      red: this.random(color.red.min, color.red.max),
      green: this.random(color.green.min, color.green.max),
      blue: this.random(color.blue.min, color.blue.max),
    }
    shapeObject.style.backgroundColor = `rgb(${this.color.red},${this.color.green},${this.color.blue})`
  }
  random(min, max) {
    return Math.floor(Math.random() * (max - min) + min)
  }
  start() {
    this.timerId = setInterval(this.tick.bind(this), slowMo)
  }
  stop() {
    clearInterval(this.timerId)
  }
  tick() {
    this.yVelocity += gravity
    this.top += this.yVelocity
    this.left += this.xVelocity
    this.shapeObject.style.top = this.top + "px"
    this.shapeObject.style.left = this.left + "px"
    if (this.top > 890 || this.left > 950) {
      this.stop()
    }
  }
}
// 子クラスball
class ball extends shape {
  constructor(shapeObject) {
    super(shapeObject)
    this.shapeObject = shapeObject
  }
}
// 子クラスsquare
class square extends shape {
  constructor(shapeObject) {
    super(shapeObject)
    this.shapeObject = shapeObject
    this.shapeObject.style.borderRadius = "10%"
    this.rotate = 0
  }
  tick() {
    this.rotate += 20
    this.yVelocity += gravity
    this.top += this.yVelocity
    this.left += this.xVelocity
    this.shapeObject.style.top = this.top + "px"
    this.shapeObject.style.left = this.left + "px"
    this.shapeObject.style.transform = `rotate(${this.rotate}deg)`
    if (this.top > 890 || this.left > 950) {
      this.stop()
    }
  }
}

// クリックイベントハンドラ
const launcher = (e) => {
  let shapeObject = document.createElement('p')
  document.getElementById('smallbox').appendChild(shapeObject)
  shapeObject.setAttribute("class", "ball")
  if (Math.random() > 0.5) {
    myshape = new ball(shapeObject)
  } else {
    myshape = new square(shapeObject)
  }
  myshape.start()
}

document.getElementById('smallbox').addEventListener('click', launcher)

